package maxima01;

public class MaximaCicleIfElse {

	public static void main(String[] args) {
		
		//таблица умножения
		for(int i = 1; i <= 10; i++) {
			for(int j = 1; j <= 10; j++) {
				System.out.print(i * j + " ");
			}
			System.out.println("");
		}
		System.out.println("");
		System.out.println("-----------Фибоначи-----------");
		MaximaCicleIfElse.fibo(21);
	}
		
	//Фибоначи
	public static long fibonazzi(byte N) { // byte нав входе ибо в long места маловато =)
		return (N <= 1) ? N : fibonazzi((byte) (N - 1)) + fibonazzi((byte) (N - 2));
	}

	public static void fibo(int N) {
		System.out.println(1);
		System.out.println(1);
		int x = 0;
		int y = 0;
		for(int i = 3; i < N; i++) {
			x= i - 2;
			y= i - 1;
			System.out.println(x + y);
		}
	}
	
}
