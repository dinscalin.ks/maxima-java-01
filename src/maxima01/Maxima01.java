package maxima01;

public class Maxima01 {

	public static void main(String[] args) {
		System.out.println("Примитивы");
		
		byte b = 1;
		short s = 2;
		int i = 4;
		long l = 8l;
		float f = 4.05F;
		double d = 8.0;
		boolean bol = true;
		char c = 'c';
		
		System.out.println("byte - " + b);
		System.out.println("short - " + s);
		System.out.println("int - " + i);
		System.out.println("long - " + l);
		System.out.println("float - " + f);
		System.out.println("double - " + d);
		System.out.println("char - " + c);
		System.out.println("boolean - " + bol);
	}

}
