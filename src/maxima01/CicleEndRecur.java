package maxima01;

public class CicleEndRecur {
    public static void main(String[] args) {
        // таблица умножения
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println("");
        }
        System.out.println("----------");
        //Фибоначи
        int x, y, fibo, temp;
        fibo = 15; // раз уж на данном этапе ввод с консоли не проходили =)
        x = 0;
        y = 1;
        temp = 1;
        System.out.print(temp + " "); // это явная залипуха =)
        for (int i = 0; i < fibo; i++) {
            temp = x + y;
            System.out.print(temp + " ");
            if (x < y) {
                x = temp;
            } else y = temp;
        }
        System.out.println("");

        x = 0; // while
        y = 1;
        while (fibo >= 0) {   //поизящнее
            temp = x + y;
            System.out.print(temp + " ");
            y = temp - y;
            x = temp;
            fibo--;
        }
        System.out.println("");
        fibo = 15;//do while
        x = 0;
        y = 1;
        do {
            temp = x + y;
            System.out.print(temp + " ");
            y = temp - y;
            x = temp;
            fibo--;
        } while (fibo >= 0);
        System.out.println("");

        System.out.println("Сумма цифр числа");
        fibo = 15984;
        int summ = 0;
        while (fibo != 0) { // использую int fibo для простоты
            summ = summ + (fibo % 10);
            fibo /= 10;
        }
        System.out.println(summ);

        System.out.println("Сумма цифр числа 2");
        fibo = 879879;
        summ = 0;
        do {
            summ = summ + (fibo % 10);
            fibo /= 10;
        } while (fibo != 0);
        System.out.println(summ);

        System.out.println("---------");
        System.out.println("=" + CicleEndRecur.sumRecursion(879879));
    }

    public static int sumRecursion(int N) {  // += а так вроде правильно
        int x = 0;
        //System.out.println("Сумма цифр числа " + N ); //
        if(N == 0) return 0;
        x =N % 10;
        System.out.print(x + "+");
        x += sumRecursion(N / 10);
        return x;
        //return (N == 0) ? 0 : N % 10 + sumRecursion(N / 10); //TODO надо в одну строку для красоты или пора таки спаать
    }
}

