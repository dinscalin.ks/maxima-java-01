package calc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) { // TODO делема
		char againWhile = '/';
		double inputA = 0;
		char inputAction = '/';
		double inputB = 0;
		Calculate calculation = new Calculate();

		do {
			System.out.println("Calculation start\n");

			System.out.println("Input first number");
			try (BufferedReader firstNum = new BufferedReader(new InputStreamReader(System.in))) {
				inputA = Double.parseDouble(firstNum.readLine());
			} catch (NumberFormatException e) { // обработка нескольких исключений
				System.out.println("You entered no number");
				e.getStackTrace();
			} catch (IOException ex) {
				ex.getStackTrace(); // TODO а если вспомню попробую с Optional если это вообще рабочий вариант
			} // TODO делема по хорошему тут finaly и Systmen.in.close()
				// НО тогда он требует throws IOException в main - а я с этим борюсь =)) ( try-with-resource - нарушением ж не будет ? =)

			calculation.setA(inputA);
			System.out.println("Input action");
			try {
				BufferedReader action = new BufferedReader(new InputStreamReader(System.in));
				inputAction = (char) action.read();
			} catch (Exception e) {
				System.out.println("Одно исключение на 2 метода");
				throw new RuntimeException(e);
			} finally {  					 // TODO попытка закрыть System.in такое себе =)))
				try {							// Обертка в Runtime результат не дала,  где то матчасть недоучил нафига такие танцы
					System.in.close();			// и чем не проверяемые легче обрабатываеть?... 
				} catch (IOException e) {
					
				}
			}

			System.out.println("Input two number");
			try (BufferedReader secondNum = new BufferedReader(new InputStreamReader(System.in))) { // метод здорового человека =)
				inputB = Double.parseDouble(secondNum.readLine());
			} catch (NumberFormatException e) { // обработка нескольких исключений
				System.out.println("You entered no number");
				e.getStackTrace();
			} catch (NullPointerException ex) { // на случай null,
				System.out.println("для Одаренных по русски: введите хоть что то =)");
				ex.getStackTrace();
			} catch (IOException ex) { // общее исключение как можно дальше от try
				ex.getStackTrace();
			} 
			calculation.setB(inputB);

			calculation.calculation(inputAction);
			calculation.getResult();

			do {
				System.out.println("\nYou want to use the calculator again? \nInput \"y\" (yes) or \"n\" (no).");
				try {
					BufferedReader again = new BufferedReader(new InputStreamReader(System.in));
					againWhile = (char) again.read();
				} catch (IOException e) {
					System.out.println("не завалить бы цикл...");
				}
			} while (againWhile != 'y' & againWhile != 'n');
		} while (againWhile == 'y');
	}
}